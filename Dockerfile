FROM ubuntu:22.10

RUN apt update
RUN apt upgrade -y
RUN apt install -y curl
RUN curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
RUN curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

RUN useradd -ms /bin/bash ubuntu

USER ubuntu

ENTRYPOINT ["/bin/bash"]
